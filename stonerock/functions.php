<?php 	

// Scripts + Styles
function enqueue_styles_scripts() {
	// wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css');
	wp_enqueue_style( 'child-style', get_stylesheet_directory_uri() . '/css/style.css');
	wp_enqueue_script( 'bootstrap', get_stylesheet_directory_uri() . '/js/bootstrap/bootstrap.js', array(), '1.0.0', true );
	wp_enqueue_script( 'bx-slider', get_stylesheet_directory_uri() . '/js/bxslider.js');

	wp_enqueue_script( 'custom-scripts', get_stylesheet_directory_uri() . '/js/script.js');
}
add_action( 'wp_enqueue_scripts', 'enqueue_styles_scripts' );

// Menus
function register_menus() {
	register_nav_menu('top-menu', 'Top Menu' );
	register_nav_menu('header-menu',__( 'Header Menu' ));
	register_nav_menu('footer-menu', 'Footer Menu' );
}
add_action( 'init', 'register_menus' );


?>