<div class="fluid-container footer">
	<footer class="container">
		
		<div class="col-lg-6 col-md-12">
		<?php 
			wp_nav_menu( array(
				'menu' 				=> 	'Footer Menu',
				'menu_class'		=>	'list-inline',
		  	)); 
		?>
		<p class="copyright">&copy; StoneRock Advisors. All Rights Reserved. Company Registration Number: 07479524</p>
		
		</div>

		<div class="col-lg-4 col-md-8 col-sm-8 col-xs-12">
			<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d2482.2339124555783!2d-0.08912359999999998!3d51.527269399999994!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x48761ca66f39e1b9%3A0xd64c7b36fddd6ec8!2sRamon+Lee+%26+Partners+Kemp+House%2C+152-160+City+Rd%2C+London+EC1V+2DW!5e0!3m2!1sen!2suk!4v1428504329333" height="190" frameborder="0" style="border:0; width: 100%; height: 190px;"></iframe>
		</div>

		<div class="col-lg-2 col-md-4 col-sm-4 col-xs-12">
			<h3>Contact</h3>
			<p>
				Kemp House, <br>
				152 – 160 City Road, <br>
				London, <br>
				EC1V 2NX
			</p>
			<span class="tel">01689 867839</span> <br>
			<span class="email"><a href="mailto:info@stonerock.global">info@stonerock.global</a></span>
		</div>

	</footer>
</div>

</body>
</html>