<!DOCTYPE html>
<html <?php language_attributes(); ?>>
	<head>

		<meta charset="<?php bloginfo( 'charset' ); ?>" />
		<meta name="viewport" content="width=device-width" />

		<title><?php wp_title( ' | ', true, 'right' ); ?></title>

		<?php wp_head(); ?>
	</head>

<body <?php body_class(); ?>>


<nav class="navbar navbar-inverse">
<div class="container">
<?php 
	wp_nav_menu( array(
		'menu' 				=> 	'Top Menu',
		'container_id'		=> 	'navbar-collapse-login',
		'container_class'	=> 	'collapse navbar-collapse',
		'menu_class'		=>	'nav navbar-nav navbar-right',
		'before'			=>	'<li><span class="email"><a href="mailto:info@stonerock.global">info@stonerock.global</a></span></li><li><span class="tel">01689 867839</span></li><li>',
		'after'				=>	'</li>'
  	)); 
?>
</div>
</nav>

<nav class="navbar navbar-default">
  <div class="container">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse-main, #navbar-collapse-login">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="#">
      	<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logo-menu.png">
      </a>
    </div>

<?php 
	wp_nav_menu( array(
		'menu'				=>	'Header Menu',
		'container_id'		=>	'navbar-collapse-main',
		'container_class'	=>	'collapse navbar-collapse',
		'menu_class'		=>	'nav navbar-nav navbar-right'
	));
?>

  </div><!-- /.container-fluid -->
</nav>