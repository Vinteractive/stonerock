<?php // Template Name: Home Page ?>

<?php get_header(); ?>
<?php layerslider(1) ?>

<div class="container info-panel">
	<div class="col-lg-4 col-md-4">
		<a href="<?php the_field('info_panel_1_link'); ?>"> 
			<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/innovators.png" alt="">
			<h2>Innovators</h2>
		</a>
		<p><?php the_field('info_panel_1'); ?></p>
	</div>
	<div class="col-lg-4 col-md-4">
		<a href="<?php the_field('info_panel_2_link'); ?>">
			<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/advisors.png" alt="">
			<h2>Advisors</h2>
		</a>
		<p><?php the_field('info_panel_2'); ?></p>
	</div>
	<div class="col-lg-4 col-md-4">
		<a href="<?php the_field('info_panel_3_link'); ?>">
			<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/accelerator.png" alt="">
			<h2>Accelerator</h2>
		</a>
		<p><?php the_field('info_panel_3'); ?></p>
	</div>
</div>

<div class="fluid-container banner">
	<div class="banner-bg">
		<div class="container">
			<h1><?php the_field('banner_heading'); ?></h1>
			<p>
				<?php the_field('banner_text'); ?>
			</p>
			
			<a href="<?php the_field('banner_read_more'); ?> ">Read more</a>

			<?php $image = get_field('banner_image'); ?>
			<img src="<?php echo $image['url']; ?>">
	
		</div>
	</div>	
</div>

<div class="container partners">
	<h3>Partners</h3>
<?php 

$images = get_field('partner_images_gallery');

if( $images ): ?>
	<div class="partners-slider">
        <?php foreach( $images as $image ): ?>
            <div class="slide">
                 <img src="<?php echo $image['sizes']['large']; ?>" alt="<?php echo $image['alt']; ?>" />
            </div>
        <?php endforeach; ?>
    </div>
<?php endif; ?>

</div>

<?php get_footer(); ?>