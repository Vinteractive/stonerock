jQuery(document).ready(function($) {

// Partners SLider
$('.partners-slider').bxSlider({
    slideWidth: 1200,
    minSlides: 4,
    maxSlides: 6,
    slideMargin: 20,
    infiniteLoop: true,
    ticker: true,
    // tickerHover: true,
    pager: false,
    controls: false,
    speed: 50000,
    responsive: true
  });

});